<?php
// src/Controller/ArticlesController.php

namespace App\Controller;


class ArticlesController extends AppController
{
    public function index()
    {
		$login_username= $this->Auth->user('id');
        $this->loadComponent('Paginator');
		 $articles = $this->Articles->find()->select(['Articles.title','Articles.created','Articles.id','Articles.slug','Articles.user_id','cnt1'=>'count(Comments.article_id)'])->leftJoinWith('Comments')->where(['Articles.user_id' => $login_username])->group('articles.id');
		$articles->order(['cnt1'=>'DESC'])->all();
       
        $this->set(compact('articles'));
    }

    public function view($slug = null)
	{
		$article = $this->Articles->findBySlug($slug)->contain(['Tags','Comments', 'Comments.Users'])->firstOrFail();
		$login_username= $this->Auth->user('email');
		$this->set(compact('login_username'));
	    $this->set(compact('article'));
	}

	public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            $article->user_id = $this->Auth->user('id');

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
        // Get a list of tags.
        $tags = $this->Articles->Tags->find('list');

        // Set tags to the view context
        $this->set('tags', $tags);

        $this->set('article', $article);
    }

    public function edit($slug)
	{
	    $article = $this->Articles->findBySlug($slug)->contain(['Tags'])->firstOrFail();
	    if ($this->request->is(['post', 'put'])) {
	        $this->Articles->patchEntity($article, $this->request->getData(), [
            // Added: Disable modification of user_id.
            'accessibleFields' => ['user_id' => false]
        ]);
	        if ($this->Articles->save($article)) {
	            $this->Flash->success(__('Your article has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your article.'));
	    }

	    // Get a list of tags.
    	$tags = $this->Articles->Tags->find('list');

    	// Set tags to the view context
    	$this->set('tags', $tags);

	    $this->set('article', $article);
	}

	public function delete($slug)
	{
	    $this->request->allowMethod(['post', 'delete']);

	    $article = $this->Articles->findBySlug($slug)->firstOrFail();
	    if ($this->Articles->delete($article)) {
	        $this->Flash->success(__('The {0} article has been deleted.', $article->title));
	        return $this->redirect(['action' => 'index']);
	    }
	}


	public function tags()
	{
	    // The 'pass' key is provided by CakePHP and contains all
	    // the passed URL path segments in the request.
	    $tags = $this->request->getParam('pass');

	    // Use the ArticlesTable to find tagged articles.
	    $articles = $this->Articles->find('tagged', [
	        'tags' => $tags
	    ]);

	    // Pass variables into the view template context.
	    $this->set([
	        'articles' => $articles,
	        'tags' => $tags
	    ]);
	}

	public function isAuthorized($user)
	{
	    $action = $this->request->getParam('action');
	    // The add and tags actions are always allowed to logged in users.
	    if (in_array($action, ['add', 'tags','view'])) {
	        return true;
	    }

	    // All other actions require a slug.
	    $slug = $this->request->getParam('pass.0');
	    if (!$slug) {
	        return false;
	    }

	    // Check that the article belongs to the current user.
	    $article = $this->Articles->findBySlug($slug)->first();

	    return $article->user_id === $user['id'];
	}
}