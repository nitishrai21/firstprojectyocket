<!-- File: src/Template/Articles/view.ctp -->

<h1><?= h($article->title) ?></h1>
<p><?= h($article->body) ?></p>
<p><b>Tags:</b> <?= h($article->tag_string) ?></p>


<p><small>Created: <?= $article->created->format(DATE_RFC850) ?></small></p>
<p><?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?></p>
<?php
    echo $this->Form->create('Post', array('url'=>array('controller'=>'Comments', 'action'=>'add')));
    echo $this->Form->control('comment', ['rows' => '3']);
    echo $this->Form->control('article_id',['type' => 'hidden', 'value' => $article->id] );
    echo $this->Form->button(__('Save Comment'));
    echo $this->Form->end();
?>
<p><b>Comment as:</b> <?= h($login_username) ?></p>
<table>
    <th>comment</th>
    <th>by user</th>
    <?php foreach ($article->comments as $comment): ?>
                
        <tr>
            <td><?= h($comment->comment) ?> </td> 
            <td><?= h($comment->user->email) ?> </td> 

        </tr>
                
    <?php endforeach; ?>
</table>